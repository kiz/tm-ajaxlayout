$j(document).ready(function() {
    /**
     *
     * @param allowFilterArray
     * @param elemHref
     * @param elemSelect
     * @param ajaxFiltersClient
     * @constructor
     */
    function FiltersAjax(allowFilterArray,elemHref,elemSelect,ajaxFiltersClient) {
        this.allowFilterArray = allowFilterArray;
        this.elemHref = elemHref;
        this.elemSelect = elemSelect;
        this.ajaxClient = ajaxFiltersClient
    }

    FiltersAjax.prototype.checkFilterStatus = function(filters) {

        var filterArr = [];
        $j.each(filters,function(index,value){
            if($j.inArray(index,filtersAjax.allowFilterArray) !== -1) {
                filterArr.push(value)
            }
        });
        if(!filterArr) {
            return filterArr;
        }

        return filterArr.join();
    };

    FiltersAjax.prototype.initAjaxFilter = function() {

        var stringFilterHref = this.checkFilterStatus(this.elemHref);
        var stringFilterSelect = this.checkFilterStatus(this.elemSelect);

        if(stringFilterHref) {
            $j(document).on('click', stringFilterHref, function (event) {
                event.preventDefault();
                var element = $j(this);
                var urlRequest = element.attr('href');
                if(urlRequest && !ajaxFiltersClient.isLoading) {
                    filtersAjax.ajaxClient.requestClient(urlRequest);
                }
            });
        }

        if(stringFilterSelect) {
            $j(document).on('change', stringFilterSelect, function (event) {
                var element = $j(this);
                var urlRequest = element.val();
                if(urlRequest && !ajaxFiltersClient.isLoading) {
                    filtersAjax.ajaxClient.requestClient(urlRequest);
                }
            })
        }

    };

    /**
     *
     * @constructor
     */
    function AjaxFiltersClient() {
        this.isLoading = false;
        this.jqxhr = null;
        this._removeSetLocationAttr();
    }

    AjaxFiltersClient.prototype._removeSetLocationAttr = function() {
        $j('.limiter select').removeAttr('onchange');
        $j('.sorter select').removeAttr('onchange');
    };

    AjaxFiltersClient.prototype.onRequest = function(data) {
        this.isLoading = false;


        if (data.error){
            if ((typeof data.message) == 'string') {
                alert(data.message);
            } else {
                alert(data.message.join("\n"));
            }
            return false;
        }

        var contentHtml = data.html;

        if (!contentHtml) {
            alert('Content is empty!');
            return false;
        }

        //Product list content set html
        $j('.category-products').html(
            $j('<div />').html(contentHtml).find('.category-products').html()
        );

        //Layered navigation filter set html.
        $j('.block-layered-nav').html(
            $j('<div />').html(contentHtml).find('.block-layered-nav').html()
        );
        ajaxFiltersClient._removeSetLocationAttr();
    };

    AjaxFiltersClient.prototype.onDone = function() {
        //console.log("On Done");
    };

    AjaxFiltersClient.prototype.onFail = function() {
        //console.log("On Fail");
    };

    AjaxFiltersClient.prototype.onAlways = function() {
        ajaxFiltersClient.isLoading = false;
        //console.log("Always");
    };

    AjaxFiltersClient.prototype.requestClient = function(urlRequest){
        ajaxFiltersClient.isLoading = true;
        this.jqxhr = $j.getJSON(urlRequest, this.onRequest)
            .done(this.onDone)
            .fail(this.onFail)
            .always(this.onAlways);
    };

    AjaxFiltersClient.prototype.getJqxhr = function() {
        return this.jqxhr;
    }

    /**
     *
     * Init classes
     *
     * var allowFilterArray use values from Tm_LayerAjax_Helper_Data::getJsValues()
     *
     * */

    var elemHref = {
        sortby : '.sort-by a',
        viewmode : '.view-mode a',
        layerattr : '.block-layered-nav a',
        pagination : '.pages li a',
    }

    var elemSelect = {
        sortby : '.sorter select',
        limiter : '.limiter select'
    }

    if(allowFilterArray) {
        var ajaxFiltersClient = new AjaxFiltersClient();
        var filtersAjax = new FiltersAjax(allowFilterArray,elemHref,elemSelect,ajaxFiltersClient);
        filtersAjax.initAjaxFilter();
    }
});






