<?php

/**
 * Created by Tm.
 */

class Tm_LayerAjax_Helper_Data extends Mage_Core_Helper_Abstract {

    const AJAX_FILTER_KEY_LIMITER = 'limiter';
    const AJAX_FILTER_KEY_SORTBY = 'sortby';
    const AJAX_FILTER_KEY_VIEWMODE = 'viewmode';
    const AJAX_FILTER_KEY_LAYERATTR = 'layerattr';
    const AJAX_FILTER_KEY_PAGINATION = 'pagination';


    protected function _getActiveFiltersArray()
    {

        $filterArr = array();

        if($limiter = Mage::getStoreConfig('tm/tm_layerajax_filter/limiter')) {
            $filterArr[] = self::AJAX_FILTER_KEY_LIMITER;
        }

        if($limiter = Mage::getStoreConfig('tm/tm_layerajax_filter/sortby')) {
            $filterArr[] = self::AJAX_FILTER_KEY_SORTBY;
        }

        if($limiter = Mage::getStoreConfig('tm/tm_layerajax_filter/viewmode')) {
            $filterArr[] = self::AJAX_FILTER_KEY_VIEWMODE;
        }

        if($limiter = Mage::getStoreConfig('tm/tm_layerajax_filter/layerattr')) {
            $filterArr[] = self::AJAX_FILTER_KEY_LAYERATTR;
        }

        if($limiter = Mage::getStoreConfig('tm/tm_layerajax_filter/pagination')) {
            $filterArr[] = self::AJAX_FILTER_KEY_PAGINATION;
        }

        return Mage::helper('core')->jsonEncode($filterArr);
    }

    public function getJsValues()
    {

        $encodedArray = $this->_getActiveFiltersArray();
        $scriptJS = <<<JSON_BIGINT_AS_STRING
        <script type="text/javascript">
            var allowFilterArray = {$encodedArray}
        </script>
JSON_BIGINT_AS_STRING;
        return $scriptJS;
    }

}