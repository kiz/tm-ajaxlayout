<?php

/**
 * Created by Tm.
 */

require_once(Mage::getModuleDir('controllers','Mage_Catalog').DS.'CategoryController.php');

class Tm_LayerAjax_CategoryController extends Mage_Catalog_CategoryController
{

    /**
     *
     * Generate custom layout for catalog category page
     *
     * @return string
     * @throws Mage_Core_Exception
     */
    protected function _getLayeredAjaxLayout()
    {
        $layout = $this->getLayout();
        $update = $layout->getUpdate();
        $update->load('tm_catalog_category_layered_ajax');
        $layout->generateXml()->generateBlocks();
        Mage::dispatchEvent(
            'controller_action_layout_generate_blocks_after',
            array('action'=>$this, 'layout'=>$this->getLayout())
        );
        return $layout->getOutput();
    }

    /**
     * @return void|Zend_Controller_Response_Abstract
     */
    public function viewAction()
    {

        if(!$this->getRequest()->isXmlHttpRequest()) {
            return parent::viewAction();
        }

        $response = array();
        $category = $this->_initCatagory();

        if(!$category) {
            $response['error'] = true;
            $response['message'] = $this->__('Can not load category!');
            $jsonData = Mage::helper('core')->jsonEncode($response);
            $this->getResponse()->setHeader('Content-type', 'application/json');
            return $this->getResponse()->setBody($jsonData);
        }

        $design = Mage::getSingleton('catalog/design');
        $settings = $design->getDesignSettings($category);

        // apply custom design
        if ($settings->getCustomDesign()) {
            $design->applyCustomDesign($settings->getCustomDesign());
        }

        try {
           $response['html'] = $this->_getLayeredAjaxLayout();
           $response['success'] = true;
        } catch(Exception $e) {
           $response['error'] = true;
           $response['message'] = $this->__('Can not finish request!');
        }

        $jsonData = Mage::helper('core')->jsonEncode($response);
        $this->getResponse()->setHeader('Content-type', 'application/json');
        return $this->getResponse()->setBody($jsonData);
    }
}